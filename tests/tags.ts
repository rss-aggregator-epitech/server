import 'ts-mocha'

import expect from 'expect'

import db from '../src/database'
import { TagCreateInput, TagsUpdateInput } from '../src/generated/types'
import { client } from '../src/keystone'
import { Lists } from '../src/types'

export default () => {
  afterEach(async () => {
    await db.drop()
  })

  const tag: TagCreateInput = {
    name: 'Category',
  }

  const fields = `
    id
    name
    _label_
  `

  it('should create tag', async () => {
    const doc = await client.createOne(Lists.TAG, tag)

    expect(doc).toBeTruthy()
  })

  it('should update tag', async () => {
    const { id } = await client.createOne(Lists.TAG, tag)

    const data: TagsUpdateInput = {
      id,
      data: {
        name: 'New-Category',
      },
    }

    const doc = await client.updateOne(Lists.TAG, data)

    expect(doc).toBeTruthy()
  })

  it('should read tag', async () => {
    const { id } = await client.createOne(Lists.TAG, tag)

    const doc = await client.getOne(Lists.TAG, id, fields)

    expect(doc).toHaveProperty('id')
    expect(doc).toHaveProperty('name')
    expect(doc).toHaveProperty('_label_', '#category')
  })

  it('should not delete tag', async () => {
    const { id } = await client.createOne(Lists.TAG, tag)

    const doc = await client.deleteOne(Lists.TAG, id)

    expect(doc).toBeFalsy()
  })
}

import 'ts-mocha'

import expect from 'expect'

import db from '../src/database'
import { FeedCreateInput, FeedsUpdateInput } from '../src/generated/types'
import { client } from '../src/keystone'
import { Lists } from '../src/types'

export default () => {
  afterEach(async () => {
    await db.drop()
  })

  const feed: FeedCreateInput = {
    name: 'My Feed',
  }

  const fields = `
    id
    name
    websites { id }
  `

  it('should create feed', async () => {
    const doc = await client.createOne(Lists.FEED, feed)

    expect(doc).toBeTruthy()
  })

  it('should update feed', async () => {
    const { id } = await client.createOne(Lists.FEED, feed)

    const data: FeedsUpdateInput = {
      id,
      data: {
        name: 'Updated Feed',
      },
    }

    const doc = await client.updateOne(Lists.FEED, data)

    expect(doc).toBeTruthy()
  })

  it('should read feed', async () => {
    const { id } = await client.createOne(Lists.FEED, feed)

    const doc = await client.getOne(Lists.FEED, id, fields)

    expect(doc).toHaveProperty('id')
    expect(doc).toHaveProperty('name')
    expect(doc).toHaveProperty('websites', [])
  })

  it('should delete feed', async () => {
    const { id } = await client.createOne(Lists.FEED, feed)

    const doc = await client.deleteOne(Lists.FEED, id)

    expect(doc).toBeTruthy()
  })
}

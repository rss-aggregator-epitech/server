import 'ts-mocha'

import expect from 'expect'

import db from '../src/database'
import { UserCreateInput, UsersUpdateInput } from '../src/generated/types'
import { client } from '../src/keystone'
import { Lists } from '../src/types'

export default () => {
  afterEach(async () => {
    await db.drop()
  })

  const user: UserCreateInput = {
    firstname: 'User',
    lastname: 'Test',
    email: 'test@mail.com',
    password: 'azertyuiop',
  }

  const fields = `
    id
    email
    firstname
    lastname
    feeds { id }
  `

  it('should create user', async () => {
    const doc = await client.createOne(Lists.USER, user)

    expect(doc).toBeTruthy()
  })

  it('should update user', async () => {
    const { id } = await client.createOne(Lists.USER, user)

    const data: UsersUpdateInput = {
      id,
      data: {
        email: 'test-2@mail.com',
        firstname: 'Updated User',
        lastname: 'Updated Test',
        password: 'azertyuiop-2',
      },
    }

    const doc = await client.updateOne(Lists.USER, data)

    expect(doc).toBeTruthy()
  })

  it('should read user', async () => {
    const { id } = await client.createOne(Lists.USER, user)

    const doc = await client.getOne(Lists.USER, id, fields)

    expect(doc).toHaveProperty('id')
    expect(doc).toHaveProperty('email')
    expect(doc).toHaveProperty('firstname')
    expect(doc).toHaveProperty('lastname')
    expect(doc).toHaveProperty('feeds', [])
    expect(doc).not.toHaveProperty('password')
  })

  it('should delete user', async () => {
    const { id } = await client.createOne(Lists.USER, user)

    const doc = await client.deleteOne(Lists.USER, id)

    expect(doc).toBeTruthy()
  })
}

import 'ts-mocha'

import expect from 'expect'

import db from '../src/database'
import { WebsiteCreateInput } from '../src/generated/types'
import { client } from '../src/keystone'
import { Lists } from '../src/types'

export default () => {
  afterEach(async () => {
    await db.drop()
  })

  const website: WebsiteCreateInput = {
    feedUrl: 'http://www.lesnumeriques.com/rss.xml',
  }

  const fields = `
    id
    title
    link
    feedUrl
    articles {
      title
      link
      pubDate
      content
      contentSnippet
      id
      image
      author
      summary
    }
    lastBuildDate
  `

  it('should create website', async () => {
    const { id: tag } = await client.createOne(Lists.TAG, { name: 'Category' })
    const doc = await client.createOne(Lists.WEBSITE, { ...website, tag: { connect: { id: tag } } })

    expect(doc).toBeTruthy()
  })

  it('should not update website', async () => {
    const { id: tag } = await client.createOne(Lists.TAG, { name: 'Category' })
    const { id } = await client.createOne(Lists.WEBSITE, { ...website, tag: { connect: { id: tag } } })

    const data = {
      id,
      data: {
        feedUrl: 'http://www.journaldugeek.com/rss',
        tag: { connect: { id: tag } },
      },
    }

    const promise = client.updateOne(Lists.WEBSITE, data)

    expect(promise).rejects.toBeTruthy()
  })

  it('should read website', async () => {
    const { id: tag } = await client.createOne(Lists.TAG, { name: 'Category' })
    const { id } = await client.createOne(Lists.WEBSITE, { ...website, tag: { connect: { id: tag } } })

    const doc = await client.getOne(Lists.WEBSITE, id, fields)

    expect(doc).toHaveProperty('id')
    expect(doc).toHaveProperty('title')
    expect(doc).toHaveProperty('link')
    expect(doc).toHaveProperty('feedUrl')
    expect(doc).toHaveProperty('articles')
    expect(doc.articles[0]).toHaveProperty('title')
    expect(doc.articles[0]).toHaveProperty('link')
    expect(doc.articles[0]).toHaveProperty('pubDate')
    expect(doc.articles[0]).toHaveProperty('content')
    expect(doc.articles[0]).toHaveProperty('contentSnippet')
    expect(doc.articles[0]).toHaveProperty('id')
    expect(doc.articles[0]).toHaveProperty('image')
    expect(doc.articles[0]).toHaveProperty('author')
    expect(doc.articles[0]).toHaveProperty('summary')
    expect(doc).toHaveProperty('lastBuildDate')
  })

  it('should delete website', async () => {
    const { id: tag } = await client.createOne(Lists.TAG, { name: 'Category' })
    const { id } = await client.createOne(Lists.WEBSITE, { ...website, tag: { connect: { id: tag } } })

    const doc = await client.deleteOne(Lists.WEBSITE, id)

    expect(doc).toBeTruthy()
  })
}

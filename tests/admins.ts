import 'ts-mocha'

import expect from 'expect'

import db from '../src/database'
import { AdminCreateInput, AdminsUpdateInput } from '../src/generated/types'
import { client } from '../src/keystone'
import { Lists } from '../src/types'

export default () => {
  afterEach(async () => {
    await db.drop()
  })

  const admin: AdminCreateInput = {
    email: 'admin@mail.com',
    firstname: 'Admin',
    lastname: 'RSS Aggregator',
    password: 'azertyuiop',
  }

  const fields = `
    id
    email
    firstname
    lastname
  `

  it('should create admin', async () => {
    const doc = await client.createOne(Lists.ADMIN, admin)

    expect(doc).toBeTruthy()
  })

  it('should update admin', async () => {
    const { id } = await client.createOne(Lists.ADMIN, admin)

    const data: AdminsUpdateInput = {
      id,
      data: {
        email: 'test-2@mail.com',
        firstname: 'Updated Admin',
        lastname: 'Updated RSS Aggregator',
        password: 'azertyuiop-2',
      },
    }

    const doc = await client.updateOne(Lists.ADMIN, data)

    expect(doc).toBeTruthy()
  })

  it('should read admin', async () => {
    const { id } = await client.createOne(Lists.ADMIN, admin)

    const doc = await client.getOne(Lists.ADMIN, id, fields)

    expect(doc).toHaveProperty('id')
    expect(doc).toHaveProperty('email')
    expect(doc).toHaveProperty('firstname')
    expect(doc).toHaveProperty('lastname')
    expect(doc).not.toHaveProperty('password')
  })

  it('should delete admin', async () => {
    const { id } = await client.createOne(Lists.ADMIN, admin)

    const doc = await client.deleteOne(Lists.ADMIN, id)

    expect(doc).toBeTruthy()
  })
}
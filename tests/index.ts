import 'ts-mocha'

import axios from 'axios'
import expect from 'expect'

import server from '../src/server'
import { run } from '../utils'
import admins from './admins'
import feeds from './feeds'
import tags from './tags'
import users from './users'
import websites from './websites'

describe('Test suite', () => {
  before(async () => {
    await server.listen()
    await run('yarn generate', { log: false })
  })

  describe('Database', () => {
    describe('User', users)
    describe('Admin', admins)
    describe('Feed', feeds)
    describe('Tag', tags)
    describe('Website', websites)
  })

  describe('Admin UI', () => {
    it('should have instance of Admin UI and redirect from "/" to "/admin"', async () => {
      const root = await axios(`http://localhost:${server.port}/`)
      const admin = await axios(`http://localhost:${server.port}/admin`)

      expect(root.data).toBeTruthy()
      expect(admin.data).toBeTruthy()
      expect(admin.data).toEqual(root.data)
    })
  })
})
require('dotenv').config()

import express from 'express'

import { AdminUIApp } from '@keystonejs/app-admin-ui'
import { GraphQLApp } from '@keystonejs/app-graphql'

import keystone, { AdminPasswordAuth, GoogleAuth } from './keystone'

const port = process.env.PORT || 8000
const admin = new AdminUIApp({ name: 'rss-aggregator', authStrategy: AdminPasswordAuth }) // TODO: isAccessAllowed
const graphql = new GraphQLApp()
const apps = [graphql, admin]
export default {
  port,
  listen: async () => {
    const app = express()
    const { middlewares } = await keystone.prepare({ apps, dev: true })
    app.use(middlewares)
    app.set('trust proxy', 1)
    app.get('/', (req, res) => res.redirect('/admin'))
    await app.listen(port)
    await keystone.connect()
  },
}
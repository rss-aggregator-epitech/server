import { Select, Text, Url } from '@keystonejs/fields'
import { ListSchema } from '@keystonejs/keystone'

import { Release } from '../../types'
import { isServer } from '../access'
import { atMetadata } from '../plugins'

const schema: ListSchema = {
  // @ts-ignore
  labelResolver: item => item.type,
  fields: {
    type: {
      type: Select,
      isUnique: true,
      isRequired: true,
      options: Object.values(Release).join(', '),
      // @ts-ignore
      dataType: 'enum',
      access: {
        update: false,
      },
    },
    version: {
      type: Text,
      isRequired: true,
    },
    url: {
      type: Url,
      isRequired: true,
    },
  },
  access: {
    read: true,
    create: isServer,
    update: isServer,
    delete: false,
  },
  plugins: [atMetadata],
}

export default schema
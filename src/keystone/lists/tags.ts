import { Text } from '@keystonejs/fields'
import { ListSchema } from '@keystonejs/keystone'

import { isAdmin } from '../access'

const schema: ListSchema = {
  // @ts-ignore
  labelResolver: item => `#${item.name.toLowerCase()}`,
  fields: {
    name: {
      type: Text,
      isUnique: true,
      isRequired: true,
    },
  },
  access: {
    create: isAdmin,
    update: isAdmin,
    delete: false,
  },
}

export default schema
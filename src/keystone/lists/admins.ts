import { Password, Text } from '@keystonejs/fields'
import { ListSchema } from '@keystonejs/keystone'

import { atMetadata } from '../plugins'

const schema: ListSchema = {
  // @ts-ignore
  labelResolver: item => `${item.firstname} ${item.lastname}`,
  fields: {
    email: {
      type: Text,
      isUnique: true,
      isRequired: true,
    },
    password: {
      type: Password,
      isRequired: true,
      rejectCommon: true,
    },
    firstname: {
      type: Text,
      isRequired: true,
    },
    lastname: {
      type: Text,
      isRequired: true,
    },
  },
  access: {
    auth: true,
  },
  plugins: [atMetadata],
}

export default schema
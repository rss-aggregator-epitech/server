// @ts-ignore
import { Password, Relationship, Text, Virtual } from '@keystonejs/fields'
import { AuthenticationContext, ListSchema } from '@keystonejs/keystone'

import db from '../../database'
import { Lists } from '../../types'
import { isAdmin, isServer } from '../access'
import { atMetadata } from '../plugins'

const schema: ListSchema = {
  // @ts-ignore
  labelResolver: item => `${item.firstname} ${item.lastname}`,
  fields: {
    email: {
      type: Text,
      isUnique: true,
      isRequired: true,
    },
    password: {
      type: Password,
    },
    firstname: {
      type: Text,
      isRequired: true,
    },
    lastname: {
      type: Text,
      isRequired: true,
    },
    googleId: {
      type: Text,
      label: 'Google ID',
      access: {
        read: args => Boolean(isAdmin(args) || isServer(args)),
        create: isServer,
        update: isServer,
      },
    },
    feeds: {
      type: Relationship,
      ref: Lists.FEED,
      many: true,
    },
    read: {
      type: Virtual,
      // @ts-ignore
      extendGraphQLTypes: [`type ArticleReference { 
        website: String!
        article: String!
      }`],
      graphQLReturnType: '[ArticleReference]',
      graphQLReturnFragment: `{
        website
        article
      }`,
      // @ts-ignore
      resolver: async doc => {
        const docs: any = await db.articles_read().find({ uid: doc.id })
        const websites = await db.websites().find({ _id: docs.map(({ websiteId }) => websiteId) })
        const read = []
        for (const doc of docs) {
          const ref: any = websites.find(({ _id }) => _id.toString() === doc.websiteId.toString())
          const website = ref.title
          const article = doc.articleLink
          read.push({ website, article })
        }
        return read
      },
    },
  },
  hooks: {
    // @ts-ignore
    validateInput: ({ operation, resolvedData: data, addValidationError: error }) => {
      if (operation === 'create' && !data.password && !data.googleId)
        return error('A password or a googleId is required')
    },
  },
  access: {
    auth: true,
    create: true,
    update: args => Boolean(isAdmin(args) || isSelf(args)),
    delete: args => Boolean(isAdmin(args) || isSelf(args)),
  },
  plugins: [atMetadata],
}
// @ts-ignore
const isSelf = ({ authentication: auth, itemId }: AuthenticationContext) => Boolean(auth.item && auth.item.id === itemId)
export default schema
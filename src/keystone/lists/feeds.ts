import { Relationship, Text } from '@keystonejs/fields'
import { ListSchema } from '@keystonejs/keystone'

import db from '../../database'
import { Lists } from '../../types'
import { isAdmin, isAuth, isServer } from '../access'
import { atMetadata } from '../plugins'

const schema: ListSchema = {
  // @ts-ignore
  labelResolver: item => item.name,
  fields: {
    name: {
      type: Text,
      isRequired: true,
      hooks: {
        validateInput: async ({ resolvedData: data, addFieldValidationError: error, context: { authedItem: user } }) => {
          if (process.env.NODE_ENV === 'test') return
          const docs: any = await db.join(Lists.USER, Lists.FEED).find({ User_left_id: user.id })
          const feedIds = docs.map(({ Feed_right_id }) => Feed_right_id.toString())
          const feeds: any = await db.feeds().find({ _id: { $in: feedIds } })
          const names = feeds.map(({ name }) => name)
          if (names.includes(data.name)) return error('feeds/duplicates-names')
        },
      },
    },
    websites: {
      type: Relationship,
      ref: Lists.WEBSITE,
      many: true,
    },
  },
  access: async (args: any) => {
    if (isAdmin(args) || isServer(args)) return true
    if (!isAuth(args)) return false
    return true
  },
  plugins: [atMetadata],
}

export default schema
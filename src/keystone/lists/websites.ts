import Parser from 'rss-parser'

// @ts-ignore
import { DateTime, Relationship, Text, Virtual } from '@keystonejs/fields'
import { ListSchema } from '@keystonejs/keystone'

import db from '../../database'
import { Lists } from '../../types'
import { isAdmin } from '../access'

const parser = new Parser()

const schema: ListSchema = {
  // @ts-ignore
  labelResolver: item => item.title,
  fields: {
    title: {
      type: Text,
      access: {
        create: false,
        update: false,
      },
    },
    link: {
      type: Text,
      isUnique: true,
      access: {
        create: false,
        update: false,
      },
    },
    feedUrl: {
      type: Text,
      isUnique: true,
      isRequired: true,
    },
    tag: {
      type: Relationship,
      ref: Lists.TAG,
      isRequired: true,
      hooks: {
        // @ts-ignore
        resolveInput: ({ operation, resolvedData: data }) => {
          if (operation === 'create' && !data.tag) throw new Error('Missing tag')
          return data.tag
        },
      },
    },
    articles: {
      type: Virtual,
      // @ts-ignore
      extendGraphQLTypes: [`type Article { 
        title: String!
        link: String!
        read: Boolean!
        pubDate: String!
        content: String!
        contentSnippet: String!
        id: String
        image: String
        author: String
        summary: String
      }`],
      graphQLReturnType: '[Article]',
      graphQLReturnFragment: `{
        title
        link
        read
        pubDate
        content
        contentSnippet
        id
        image
        author
        summary
      }`,
      // @ts-ignore
      resolver: async (doc, _, { authedItem: user }) => {
        const feed = await parser.parseURL(doc.feedUrl)
        const regex = /<img[^>]+src="?([^"\s]+)"?[^>]*\/>/
        if (process.env.NODE_ENV === 'test') return feed.items
        const read: any = await db.articles_read().find({ uid: user.id, websiteId: doc.id })
        for (const article of feed.items) {
          const parts = regex.exec(article.content)
          // eslint-disable-next-line prefer-destructuring
          if (parts) article.image = parts[1]
          const found = read.find(({ articleLink }) => article.link === articleLink)
          article.read = !!found
        }
        return feed.items
      },
      access: {
        read: args => !isAdmin(args),
      },
    },
    lastBuildDate: {
      type: DateTime,
      access: {
        create: false,
        update: false,
      },
    },
  },
  hooks: {
    resolveInput: async ({ resolvedData: data }) => {
      if (!data.feedUrl) return data
      try {
        const feed = await parser.parseURL(data.feedUrl)
        data.title = feed.title
        data.link = feed.link
        data.lastBuildDate = new Date(feed.lastBuildDate).toISOString()
      } catch (e) {
        throw new Error('Invalid RSS Feed')
      }
      return data
    },
  },
  access: {
    create: isAdmin,
    update: false,
    delete: isAdmin,
  },
}

export default schema
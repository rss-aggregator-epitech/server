require('dotenv').config()

import { MongooseAdapter } from '@keystonejs/adapter-mongoose'

const mongoUri = process.env.MONGODB_URI
export const MongoDB = new MongooseAdapter({ mongoUri })
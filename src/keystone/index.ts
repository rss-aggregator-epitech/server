import MongoStore from 'connect-mongo'

import { Keystone } from '@keystonejs/keystone'

import db from '../database'
import { Lists, Release } from '../types'
import { MongoDB } from './adapters'
import AdminPasswordAuthStrategy from './auth/AdminPasswordAuthStrategy'
import GoogleAuthStrategy from './auth/GoogleAuthStrategy'
import UserPasswordAuthStrategy from './auth/UserPasswordAuthStrategy'
import ServerSideClient from './client'
import { onConnect } from './init'
import admins from './lists/admins'
import feeds from './lists/feeds'
import releases from './lists/releases'
import tags from './lists/tags'
import users from './lists/users'
import websites from './lists/websites'

const keystone = new Keystone({
  adapter: MongoDB,
  cookieSecret: process.env.KEYSTONE_SECRET, // Can be generate with: openssl rand -hex 48
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
    sameSite: false,
  },
  sessionStore: MongoStore.create({ mongoUrl: process.env.MONGODB_URI }),
  // @ts-ignore
  onConnect,
})
/**
 * Lists
 */
keystone.createList(Lists.ADMIN, admins)
keystone.createList(Lists.USER, users)
keystone.createList(Lists.TAG, tags)
keystone.createList(Lists.FEED, feeds)
keystone.createList(Lists.WEBSITE, websites)
keystone.createList(Lists.RELEASE, releases)
/**
 * Custom GraphQL
 */
keystone.extendGraphQLSchema({
  queries: [{
    schema: 'healthy: Boolean',
    resolver: () => true,
  }],
  mutations: [
    {
      schema: 'updateReleaseURL(type: ReleaseTypeType!, version: String!, url: String!): Boolean!',
      resolver: async (_, { type, version, url }, { req }) => {
        if (req.headers.authorization !== process.env.CI_SECRET)
          throw new Error('auth/forbidden')
        await db.releases().updateOne({ type }, { $set: { version, url } }, { upsert: true })
        return true
      },
    },
    {
      schema: 'markArticleAsRead(websiteId: ID!, articleLink: String!): Boolean!',
      resolver: async (_, { websiteId, articleLink }, { authedItem }) => {
        if (!authedItem) throw new Error('auth/forbidden')
        const data = {
          uid: authedItem.id,
          websiteId,
          articleLink,
        }
        await db.articles_read().create(data)
        return true
      },
    },
  ],
  types: [
    { type: `enum Releases { ${Object.values(Release)} }` },
  ],
})
export const GoogleAuth = GoogleAuthStrategy(keystone)
export const UserPasswordAuth = UserPasswordAuthStrategy(keystone)
export const AdminPasswordAuth = AdminPasswordAuthStrategy(keystone)
export const client = ServerSideClient(keystone)
export default keystone
import { bold, dim, green } from 'chalk'

import { Keystone } from '@keystonejs/keystone'
import { createItem } from '@keystonejs/server-side-graphql-client'

import db, { InitJoinSchema } from '../database'
import { Lists, Release } from '../types'
import { client } from './'

export const onConnect = async (keystone: Keystone) => {
  if (!process.env.KEYSTONE_SECRET) throw new Error('Missing "KEYSTONE_SECRET" environnement variable.')
  if (!process.env.ADMIN_EMAIL) throw new Error('Missing "ADMIN_EMAIL" environnement variable.')
  if (!process.env.ADMIN_PASSWORD) throw new Error('Missing "ADMIN_PASSWORD" environnement variable.')
  if (!process.env.MONGODB_URI) throw new Error('Missing "MONGODB_URI" environnement variable.')
  // Init join schemas
  InitJoinSchema(Lists.USER, Lists.FEED)
  InitJoinSchema(Lists.FEED, Lists.WEBSITE)
  const email = process.env.ADMIN_EMAIL
  const password = process.env.ADMIN_PASSWORD
  // Create server-side client context
  const context = keystone.createContext({
    // @ts-ignore
    authentication: { item: { id: process.env.KEYSTONE_SECRET } },
    skipAccessControl: true,
  })
  // Fetch master admin
  const doc = await db.admins().findOne({ email })
  // Auto-create master admin
  if (!doc) {
    await createItem({
      keystone,
      context,
      listKey: Lists.ADMIN,
      item: {
        email,
        password,
        firstname: 'Admin',
        lastname: 'RSS Aggregator',
      },
    })
  }
  if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging') {
    // Print master admin credentials
    console.log()
    console.log(`${green('✔')} ${bold('Master Admin:')}`)
    console.log(`➜ ${dim('Email:   ')} ${email}`)
    console.log(`➜ ${dim('Password:')} ${password}`)
  }
}
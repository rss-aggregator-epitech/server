import { AuthenticationContext } from '@keystonejs/keystone'

import { Lists } from '../types'

// @ts-ignore
export const isAdmin = ({ authentication: { listKey } }: AuthenticationContext) => Boolean(listKey === Lists.ADMIN)
export const isServer = ({ authentication: auth }: AuthenticationContext) => Boolean(auth.item && auth.item.id === process.env.KEYSTONE_SECRET)
export const isAuth = ({ authentication: auth }: AuthenticationContext) => Boolean(auth.item)
import { atTracking, AtTrackingPluginProvider } from '@keystonejs/list-plugins'

export const atMetadata: AtTrackingPluginProvider = atTracking({
  access: {
    read: true,
    create: false,
    update: false,
    delete: false,
  },
})
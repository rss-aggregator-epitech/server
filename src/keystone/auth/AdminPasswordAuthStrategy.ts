import { PasswordAuthStrategy } from '@keystonejs/auth-password'
import { Keystone } from '@keystonejs/keystone'

import { Lists } from '../../types'

const protectIdentities = process.env.NODE_ENV === 'production'

const AdminPasswordAuthStrategy = (keystone: Keystone) => {
  return keystone.createAuthStrategy({
    type: PasswordAuthStrategy,
    list: Lists.ADMIN,
    config: { protectIdentities },
  })
}

export default AdminPasswordAuthStrategy
import axios from 'axios'
import { OAuth2Client } from 'google-auth-library'
import PassportGoogle from 'passport-google-oauth20'

import { PassportAuthStrategy } from '@keystonejs/auth-passport'
import { Keystone } from '@keystonejs/keystone'

import { client } from '../'
import { Lists } from '../../types'

class GoogleAuthStrategy extends PassportAuthStrategy {
  constructor(keystone, listKey, config) {
    super(
      // @ts-ignore
      GoogleAuthStrategy.authType,
      keystone,
      listKey,
      {
        scope: ['profile', 'email'],
        ...config,
      },
      PassportGoogle
    )
  }

  async validate(resolvedData: { itemId: string; accessToken: string }) {
    const { accessToken } = resolvedData

    // @ts-ignore
    const clientId = this._passportStrategy._oauth2._clientId
    // @ts-ignore
    const clientSecret = this._passportStrategy._oauth2._clientSecret

    // Get email & googleId from access Token
    const oAuthClient = new OAuth2Client(clientId, clientSecret)

    const tokenInfos = await oAuthClient.getTokenInfo(accessToken)
    const { email } = tokenInfos
    const googleId = tokenInfos.sub

    const fields = 'id, email, name, googleId'

    try {
      // Check if user with this googleId exists
      const foundWithGoogle = await client.getMany(Lists.USER, { googleId }, fields)
      if (foundWithGoogle.length) {
        const user = foundWithGoogle[0]
        return { item: user, success: true, message: 'Authentication Successful' }
      }

      // if not, check if user with this email exists
      const foundWithEmail = await client.getMany(Lists.USER, { email }, fields)
      if (foundWithEmail.length) {
        let user = foundWithEmail[0]
        // update googleId for this user
        user = await client.updateOne(Lists.USER, { id: user.id, data: { googleId } }, fields)
        return { item: user, success: true, message: 'Authentication Successful' }
      }

      const userInfosRes = await axios({ method: 'get', url: 'https://www.googleapis.com/oauth2/v3/userinfo', headers: { Authorization: `Bearer ${accessToken}` } })
      const firstname = userInfosRes.data.given_name
      const lastname = userInfosRes.data.family_name

      // if not, create user
      const user = await client.createOne(Lists.USER, { email, firstname, lastname, googleId }, fields)
      return { item: user, success: true, message: 'Authentication Successful' }
    } catch (error) {
      console.warn(error)
      return { item: null, success: false, message: error }
    }
  }
}

// @ts-ignore
GoogleAuthStrategy.authType = 'google'

export const UserGoogleStrategy = (keystone: Keystone) => {
  return keystone.createAuthStrategy({
    type: GoogleAuthStrategy,
    list: Lists.USER,
    config: {
      idField: 'googleId',
      appId: process.env.GOOGLE_APP_ID,
      appSecret: process.env.GOOGLE_SECRET,
      loginPath: '/auth/google',
      callbackPath: '/auth/google/callback',
      callbackHost: 'http://localhost:8000',

      onAuthenticated: ({ token, item, isNewItem }, req, res) => {
        res.redirect('/')
      },

      onError: (error, req, res) => {
        console.error(error)
        res.redirect('/')
      },
    },
  })
}

export default UserGoogleStrategy
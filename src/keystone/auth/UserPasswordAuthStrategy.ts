import { PasswordAuthStrategy } from '@keystonejs/auth-password'
import { Keystone } from '@keystonejs/keystone'

import { Lists } from '../../types'

const protectIdentities = process.env.NODE_ENV === 'production'

const UserPasswordAuthStrategy = (keystone: Keystone) => {
  return keystone.createAuthStrategy({
    type: PasswordAuthStrategy,
    list: Lists.USER,
    config: { protectIdentities },
  })
}

export default UserPasswordAuthStrategy
import { Keystone } from '@keystonejs/keystone'
import {
  createItem, createItems, deleteItem, deleteItems, getItem, getItems, updateItem, updateItems,
} from '@keystonejs/server-side-graphql-client'

import { Lists } from '../types'

export default (keystone: Keystone) => {
  const context = keystone.createContext({
    // @ts-ignore
    authentication: { item: { id: process.env.KEYSTONE_SECRET } },
    skipAccessControl: true,
  })
  return {
    context,
    createOne: async (list: Lists, data: any, fields?: string) => createItem({
      keystone,
      context,
      listKey: list,
      item: data,
      returnFields: fields,
    }),
    createMany: async (list: Lists, datas: any[], fields?: string) => createItems({
      keystone,
      context,
      listKey: list,
      items: datas,
      returnFields: fields,
    }),
    getOne: async (list: Lists, id: string, fields?: string) => getItem({
      keystone,
      context,
      listKey: list,
      itemId: id,
      returnFields: fields,
    }),
    getMany: async (list: Lists, where: any, fields?: string) => getItems({
      keystone,
      context,
      listKey: list,
      where,
      returnFields: fields,
    }),
    updateOne: async (list: Lists, item: any, fields?: string) => updateItem({
      keystone,
      context,
      listKey: list,
      item,
      returnFields: fields,
    }),
    updateMany: async (list: Lists, items: any[], fields?: string) => updateItems({
      keystone,
      listKey: list,
      items,
      returnFields: fields,
    }),
    deleteOne: async (list: Lists, id: string, fields?: string) => deleteItem({
      keystone,
      listKey: list,
      itemId: id,
      returnFields: fields,
    }),
    deleteMany: async (list: Lists, items: string[], fields?: string) => deleteItems({
      keystone,
      listKey: list,
      items,
      returnFields: fields,
    }),
  }
}
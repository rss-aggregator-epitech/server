import { Schema } from 'mongoose'

export const ArticleReadSchema = new Schema({
  uid: {
    type: 'ObjectId',
    required: true,
  },
  websiteId: {
    type: 'ObjectId',
    required: true,
  },
  articleLink: {
    type: 'String',
    required: true,
  },
})
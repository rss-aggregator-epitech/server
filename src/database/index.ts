import { Mongoose, Schema } from 'mongoose'

import { MongoDB } from '../keystone/adapters'
import { Collections, Lists } from '../types'
import { ArticleReadSchema } from './articles-read'
import { JoinName, JoinSchema } from './joins'

// @ts-ignore
const db = MongoDB.mongoose as Mongoose
export const InitJoinSchema = (left: Lists, right: Lists) => {
  const name = JoinName(left, right)
  db.model(name, JoinSchema(left, right))
}
export default {
  admins: () => db.model(Lists.ADMIN),
  feeds: () => db.model(Lists.FEED),
  websites: () => db.model(Lists.WEBSITE),
  tags: () => db.model(Lists.TAG),
  releases: () => db.model(Lists.RELEASE),
  articles_read: () => db.model(Collections.ARTICLE_READ, ArticleReadSchema),
  join: (left: Lists, right: Lists) => db.model(JoinName(left, right)),
  drop: () => db.connection.dropDatabase(),
}
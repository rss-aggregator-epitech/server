import { Schema } from 'mongoose'

import { Lists } from '../../types'

export const JoinSchema = (left: Lists, right: Lists) => new Schema({
  [`${left}_left_id`]: {
    type: 'ObjectId',
    required: true,
  },
  [`${right}_right_id`]: {
    type: 'ObjectId',
    required: true,
  },
})

export const JoinName = (left: Lists, right: Lists) => `${left.toLowerCase()}_${right.toLowerCase()}s_manies`
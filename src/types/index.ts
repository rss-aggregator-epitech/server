// Lists defines the names of each Keystone List
// A Keystone List is also a MongoDB Model
export enum Lists {
    USER = 'User',
    ADMIN = 'Admin',
    TAG = 'Tag',
    FEED = 'Feed',
    WEBSITE = 'Website',
    RELEASE = 'Release'
}
// Collections defines the names of each MongoDB Model not in Keystone Lists
export enum Collections {
    ARTICLE_READ = 'article-read',
}
export enum Release {
    MOBILE = 'mobile',
    DESKTOP_MACOS = 'desktop_macos',
    DESKTOP_LINUX = 'desktop_linux',
}
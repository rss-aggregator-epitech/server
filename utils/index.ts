import { bold, dim } from 'chalk'
import { exec } from 'child_process'

export type RunOptions = {
  log?: boolean
  dim?: boolean
  prefix?: string
}

export const run = async (cmd: string, options: RunOptions = { log: true }) => {
  const child = exec(cmd)

  return new Promise<void>(next => {
    if (!child.stdout)
      throw new Error('Can not access stdout')

    if (options.log) {
      child.stdout.on('data', data => {
        let out = options.prefix ? bold(options.prefix) : ''

        const str = data.toString() as string
        out += options.dim ? dim(str) : str

        process.stdout.write(out)
      })
    }

    child.on('close', () => next())
  })
}

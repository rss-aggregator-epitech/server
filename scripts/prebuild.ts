import server from '../src/server'
import { run } from '../utils'

async function main() {
  let code = 0

  try {
    await server.listen()
    await run('yarn generate')
  } catch (e) {
    console.error(e)
    code = 1
  } finally {
    process.exit(code)
  }
}

main()
